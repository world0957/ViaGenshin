package core

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"sort"
	"strings"

	"github.com/Jx2f/ViaGenshin/pkg/logger"
)

const (
	consoleUid         = uint32(1)
	consoleNickname    = "Ayaka"
	consoleLevel       = uint32(60)
	consoleWorldLevel  = uint32(8)
	consoleSignature   = ""
	consoleNameCardId  = uint32(210081)
	consoleAvatarId    = uint32(10000002)
	consoleCostumeId   = uint32(0)
	consoleWelcomeText = "Welcome to GIO Ayaka, still experimental. please use GIO version command, using GC command does not work."
)

type MuipResponseBody struct {
	Retcode int32  `json:"retcode"`
	Msg     string `json:"msg"`
	Ticket  string `json:"ticket"`
	Data    struct {
		Msg    string `json:"msg"`
		Retmsg string `json:"retmsg"`
	} `json:"data"`
}

func (s *Server) ConsoleExecute(cmd, uid uint32, text string) (string, error) {
	logger.Info().Uint32("uid", uid).Msgf("Console Execute: %s", text)
	var values []string
	values = append(values, fmt.Sprintf("cmd=%d", cmd))
	values = append(values, fmt.Sprintf("uid=%d", uid))
	values = append(values, fmt.Sprintf("msg=%s", text))
	values = append(values, fmt.Sprintf("region=%s", s.config.Console.MuipRegion))
	ticket := make([]byte, 16)
	if _, err := rand.Read(ticket); err != nil {
		return "", fmt.Errorf("Unable to generate ticket: %w", err)
	}
	values = append(values, fmt.Sprintf("ticket=%x", ticket))
	if s.config.Console.MuipSign != "" {
		shaSum := sha256.New()
		sort.Strings(values)
		shaSum.Write([]byte(strings.Join(values, "&") + s.config.Console.MuipSign))
		values = append(values, fmt.Sprintf("sign=%x", shaSum.Sum(nil)))
	}
	uri := s.config.Console.MuipEndpoint + "?" + strings.ReplaceAll(strings.Join(values, "&"), " ", "+")
	logger.Debug().Msgf("Muip response: %s", uri)
	resp, err := http.Get(uri)
	if err != nil {
		return "Muip response: %s" + "\nKind tips" + consoleWelcomeText, err
	}
	defer resp.Body.Close()
	p, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	logger.Debug().Msgf("Muip response: %s", string(p))
	if resp.StatusCode != 200 {
		return "Muip response: %s" + "\nKind tips" + consoleWelcomeText, fmt.Errorf("status code: %d", resp.StatusCode)
	}
	body := new(MuipResponseBody)
	if err := json.Unmarshal(p, body); err != nil {
		return "", err
	}
	if (text == "help") {
		return "Enter GIO command here", nil
	}
	if body.Retcode != 0 {
		return "Failed to execute command: " + body.Data.Msg + ", mistake: " + body.Msg + "\nKind tips" + consoleWelcomeText, nil
	}
	return "Command execution successful: " + body.Data.Msg + "\nKind tips" + consoleWelcomeText, nil
}
